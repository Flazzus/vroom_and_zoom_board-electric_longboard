# Vroom_and_Zoom_Board - Electric_Longboard

Design and implementation of the Vroom & Zoom board, an electric longboard.  This was my senior project for my Electrical Engineering degree.  Included is my final report, poster, and design work, and pictures.

![image info](./Pictures/Finished_1.jpg)

## Project Description

The purpose of this project was to design a fully electric longboard.  An electric longboard is like a typical push longboard, but with extra features.  The benefit of having an electric longboard is to allow the user to ride and accelerate without having to use their foot to propel themselves forward.  Another benefit is the inclusion of a braking system.  This enables the user to go down hills while maintaining a safe and controlled speed.  The electric longboard will be powered by rechargeable lithium-ion batteries (10S 2P) and propelled by a single 3500-watt belt motor.  The motor and battery were designed in tandem to ensure range and speed requirements are met.  A speed control will be used to allow the user to ride at different velocities and to control the power transfer.  Control systems will be implemented for the safety of the rider.  This ensures that the longboard does not accelerate too fast or slow, causing the rider to be thrown off.  The electric longboard will be a fun and practical upgrade to a regular longboard.  This enables the rider to go further, up and down hills, and ride without pushing manually.


## Documents Included

1. Final_Report
    * Full report of the project.  Includes motivation, methods, design, problems encountered, and conclusion.
2. Poster
    * Presentation poster used for senior design presentation.
3. UserGuide
    * User Guide for the Electric Longboard.  Includes warnings, safety, operation instructions, and troubleshooting.
4. Pictures
    * Pictures of finished longboard and its components
5. Wiring_Diagram
    * Wiring diagram of how battery and BMS were wired together.



Tyler George
5/24/2022
